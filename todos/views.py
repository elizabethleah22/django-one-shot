from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoItemForm, TodoListForm

# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }

    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "todo_lists/detail.html", context)
